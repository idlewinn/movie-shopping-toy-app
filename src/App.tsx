import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import CartPage from "./components/cart/CartPage";
import ConfirmationPage from "./components/confirmation/ConfirmationPage";
import MovieContextProvider from "./components/MovieContextProvider";
import SearchPage from "./components/search/SearchPage";

function App() {
  // TODO: add 404 not found for unexpected paths
  return (
    <Router>
      <MovieContextProvider>
        <Switch>
          <Route path="/cart">
            <CartPage />
          </Route>
          <Route path="/confirmation">
            <ConfirmationPage />
          </Route>
          <Route path="/">
            <SearchPage />
          </Route>
        </Switch>
      </MovieContextProvider>
    </Router>
  );
}

export default App;
