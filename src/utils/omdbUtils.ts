import axios, { AxiosResponse } from "axios";

// not hardcoded because it will be publicly visible in the repo
const API_KEY = process.env.REACT_APP_OMDB_API_KEY;
const OMDB_URL = "https://www.omdbapi.com";

export async function sendQuery(query: string): Promise<AxiosResponse> {
  // encoding in case a user uses special characters in their search
  const encodedQuery = encodeURIComponent(query);
  return await axios.get(`${OMDB_URL}/?apikey=${API_KEY}&s=${encodedQuery}`);
}
