import styled from "styled-components";
import CenteredContainer from "./CenteredContainer";

const CenteredText = styled(CenteredContainer)`
  margin: 16px;
  align-items: center;
`;

export default CenteredText;
