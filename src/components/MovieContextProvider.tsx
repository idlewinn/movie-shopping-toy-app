import React, { ReactNode, useMemo, useState } from "react";
import { SearchResult } from "./search/SearchQueryTypes";

interface Props {
  children: ReactNode;
}

export const MovieContext = React.createContext<{
  selectedItems: Map<string, SearchResult>;
  setSelectedItems: (selectedItems: Map<string, SearchResult>) => void;
}>({
  selectedItems: new Map(),
  setSelectedItems: (selectedItems: Map<string, SearchResult>) => null,
});

function MovieContextProvider({ children }: Props) {
  const [selectedItems, setSelectedItems] = useState<Map<string, SearchResult>>(
    new Map()
  );

  const context = useMemo(
    () => ({
      selectedItems,
      setSelectedItems: (selectedItems: Map<string, SearchResult>) =>
        setSelectedItems(selectedItems),
    }),
    [selectedItems, setSelectedItems]
  );
  return (
    <MovieContext.Provider value={context}>{children}</MovieContext.Provider>
  );
}

export default MovieContextProvider;
