import styled from "styled-components";

interface Props {
  label: string;
  value: string;
}

const Container = styled.div`
  display: flex;
  margin: 8px;
  justify-content: space-between;
`;

const LabelValue = styled.div`
  text-align: right;
`;
function SearchResultCardTextLabel({ label, value }: Props) {
  return (
    <Container>
      <div>
        <b>
          {label}
          {": "}
        </b>
      </div>
      <LabelValue>{value}</LabelValue>
    </Container>
  );
}

export default SearchResultCardTextLabel;
