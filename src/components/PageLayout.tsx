import { ReactNode } from "react";
import styled from "styled-components";
import CenteredContainer from "./CenteredContainer";

const Root = styled(CenteredContainer)`
  align-items: stretch;
  min-height: 400px;
  @media (max-width: 414px) {
    min-height: 100vh;
  }
`;

const Section = styled(CenteredContainer)`
  padding: 4px;
  align-items: center;
`;

const Header = styled(Section)`
  background-color: white;
  position: sticky;
  top: 0;
  border-bottom: 1px solid lightgray;
`;

const Main = styled(Section)`
  flex-grow: 1;
`;

const Footer = styled(Section)`
  @media (max-width: 414px) {
    background-color: white;
    border-top: 1px solid lightgray;
    position: sticky;
    bottom: 0;
  }
`;

const Content = styled(CenteredContainer)`
  justify-content: center;
  flex-grow: 1;
  width: 100%;
  max-width: 1280px;
`;

const MainContent = styled(Content)`
  justify-content: flex-start;
  background-color: whitesmoke;
  border: 1px solid lightgray;
  border-radius: 8px;
`;

interface Props {
  header: ReactNode;
  children: ReactNode;
  footer?: ReactNode;
}

function PageLayout({ children, footer, header }: Props) {
  return (
    <Root>
      <Header>
        <Content>{header}</Content>
      </Header>
      <Main>
        <MainContent>{children}</MainContent>
      </Main>
      {footer != null ? (
        <Footer>
          <Content>{footer}</Content>
        </Footer>
      ) : null}
    </Root>
  );
}

export default PageLayout;
