import { useHistory } from "react-router";
import styled from "styled-components";

import Button from "../Button";

const ReturnHome = styled(Button)`
  background-color: dodgerblue;
  color: white;
  font-weight: bold;
  padding: 8px;
  height: 60px;
  :active {
    background-color: cornflowerblue;
  }
  @media (min-width: 415px) {
    width: 200px;
    align-self: flex-end;
  }
`;

function ReturnHomeButton() {
  const history = useHistory();

  return (
    <ReturnHome onClick={() => history.push("/")}>Continue Shopping</ReturnHome>
  );
}

export default ReturnHomeButton;
