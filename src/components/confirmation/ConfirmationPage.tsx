import styled from "styled-components";
import CenteredContainer from "../CenteredContainer";
import Logo from "../Logo";
import PageLayout from "../PageLayout";
import ReturnHomeButton from "./ReturnHomeButton";
import CenteredText from "../CenteredText";

const HeaderContainer = styled.div`
  display: flex;
  align-items: center;
  flex-direction: column;
  align-items: stretch;
`;

const CartHeader = styled(CenteredContainer)`
  text-align: center;
  font-size: 24px;
`;

function ConfirmationPage() {
  return (
    <PageLayout
      header={
        <HeaderContainer>
          <Logo />
          <CartHeader>CONFIRMATION</CartHeader>
        </HeaderContainer>
      }
      footer={<ReturnHomeButton />}
    >
      <CenteredText>Thanks for placing your order!</CenteredText>
    </PageLayout>
  );
}

export default ConfirmationPage;
