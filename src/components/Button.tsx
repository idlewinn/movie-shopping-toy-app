import styled from "styled-components";

// a clean button to disable default styles and force us
// to style properly while keeping the general benefits
// of using html button
const Button = styled.button`
  background: none;
  border-radius: 8px;
  color: inherit;
  border: none;
  padding: 0;
  font: inherit;
  cursor: pointer;
  outline: 0;
  :focus-visible {
    outline: 4px solid blue;
  }
`;

export default Button;
