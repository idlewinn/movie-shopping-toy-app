import styled from "styled-components";

const LogoContainer = styled.div`
  font-size: 24px;
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  font-weight: bold;
  white-space: nowrap;
  @media (max-width: 414px) {
    margin-bottom: 8px;
  }
`;

const MovieText = styled.span`
  color: red;
`;
const ShopperText = styled.span`
  color: blue;
`;

function Logo() {
  return (
    <LogoContainer>
      <MovieText>MOVIE</MovieText>
      <ShopperText>SHOPPER</ShopperText>
    </LogoContainer>
  );
}

export default Logo;
