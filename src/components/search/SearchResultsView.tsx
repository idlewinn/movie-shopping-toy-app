import type { SearchResponse } from "./SearchQueryTypes";

import styled from "styled-components";

import SearchResultCard from "./SearchResultCard";
import CenteredContainer from "../CenteredContainer";
import CenteredText from "../CenteredText";

const ContentContainer = styled(CenteredContainer)`
  @media (min-width: 415px) {
    align-content: stretch;
    flex-direction: row;
    flex-wrap: wrap;
  }
`;

const Banner = styled.div`
  border: 1px solid lightgray;
  border-radius: 8px;
  margin: 8px;
  background-color: white;
  padding: 8px;
`;

const TotalResults = styled.div`
  margin-bottom: 8px;
`;

const LoadingSpinnerContainer = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
`;

const LoadingSpinner = styled.img`
  width: 64px;
  height: 64px;
`;

interface Props {
  isErrored: boolean;
  isQuerying: boolean;
  queryData: SearchResponse;
}

function FailedSearchResult({ error }: { error?: string | null }) {
  switch (error) {
    case "Too many results.":
      return (
        <CenteredText>
          Too many results. Please refine your search!
        </CenteredText>
      );
    case "Movie not found!":
      return (
        <CenteredText>
          No movies found with your search. Please try again!
        </CenteredText>
      );
    default:
      return (
        <CenteredText>Something went wrong. Please try again!</CenteredText>
      );
  }
}

function SearchResultsView({ isErrored, isQuerying, queryData }: Props) {
  if (isQuerying) {
    return (
      <LoadingSpinnerContainer>
        <LoadingSpinner alt="loading indicator" src="loading.gif" />
      </LoadingSpinnerContainer>
    );
  }
  if (isErrored) {
    return <FailedSearchResult />;
  }

  if (queryData == null) {
    return <CenteredText>Please search for some movies!</CenteredText>;
  }

  if (queryData.Response === "False") {
    return <FailedSearchResult error={queryData.Error} />;
  }

  const { Search: search, totalResults } = queryData;

  return (
    <>
      <Banner>
        <TotalResults>
          <b>{totalResults} results found. </b>
          <i>
            {Number(totalResults) > search.length
              ? `Only showing the first ${search.length} results.`
              : null}
          </i>
        </TotalResults>
        Please select one or more movies.
      </Banner>
      <ContentContainer>
        {search.map((resultData) => (
          <SearchResultCard key={resultData.imdbID} {...resultData} />
        ))}
      </ContentContainer>
    </>
  );
}

export default SearchResultsView;
