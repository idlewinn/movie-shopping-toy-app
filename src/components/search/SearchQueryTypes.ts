export interface SearchResult {
  Title: string;
  Year: string;
  imdbID: string;
  Type: string;
  Poster: string;
}

interface SearchSuccessResponse {
  Response: "True";
  Search: SearchResult[];
  totalResults: string;
}

interface SearchFailureResponse {
  Response: "False";
  Error: string;
}

export type SearchResponse =
  | SearchSuccessResponse
  | SearchFailureResponse
  | null;
