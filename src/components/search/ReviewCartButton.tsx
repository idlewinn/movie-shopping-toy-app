import { useContext } from "react";
import { useHistory } from "react-router";
import styled from "styled-components";
import Button from "../Button";
import { MovieContext } from "../MovieContextProvider";

const ReviewCart = styled(Button)`
  background-color: ${(props) => (props.disabled ? "lightgray" : "green")};
  color: white;
  font-weight: bold;
  padding: 8px;
  height: 60px;
  :active {
    background-color: lightgreen;
  }
  @media (min-width: 415px) {
    width: 200px;
    align-self: flex-end;
  }
`;

function ReviewCartButton() {
  const history = useHistory();
  const { selectedItems } = useContext(MovieContext);
  const itemCount = selectedItems.size;
  const itemsText = itemCount !== 1 ? "items" : "item";

  return (
    <ReviewCart
      disabled={itemCount === 0}
      onClick={() => history.push("/cart")}
    >
      Review Cart ({itemCount} {itemsText})
    </ReviewCart>
  );
}

export default ReviewCartButton;
