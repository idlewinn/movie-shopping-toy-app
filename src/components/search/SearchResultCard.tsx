import { useCallback, useContext } from "react";
import styled from "styled-components";
import Button from "../Button";
import { MovieContext } from "../MovieContextProvider";
import type { SearchResult } from "./SearchQueryTypes";
import CardTextLabel from "../CardTextLabel";

const Card = styled(Button)<{ isSelected: boolean }>`
  min-width: 300px;
  border: ${(props) =>
    props.isSelected ? "8px solid dodgerblue" : "1px solid lightgray"};
  border-radius: 8px;
  background-color: ${(props) => (props.isSelected ? "lightcyan" : "white")};
  margin: 0 8px 8px 8px;
  display: flex;
  flex-grow: 1;
  flex-shrink: 1;
  flex-basis: 0;
  padding: ${(props) => (props.isSelected ? "1px" : "8px")};
`;

const CardContent = styled.div`
  flex-grow: 1;
  margin: 24px;
  text-align: start;
`;

const PosterImage = styled.img`
  margin: 8px;
  height: 160px;
  object-fit: contain;
  max-width: 30%;
`;

const PosterImagePlaceholder = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  padding: 8px;
  height: 160px;
  border: 1px solid gray;
  width: 30%;
`;

function SearchResultCard(props: SearchResult) {
  const { selectedItems, setSelectedItems } = useContext(MovieContext);

  const onSelectItem = useCallback(
    (result: SearchResult) => {
      const imdbID = result.imdbID;
      const selectedItemsCopy = new Map(selectedItems);
      if (selectedItemsCopy.has(imdbID)) {
        selectedItemsCopy.delete(imdbID);
      } else {
        selectedItemsCopy.set(imdbID, result);
      }
      setSelectedItems(selectedItemsCopy);
    },
    [selectedItems, setSelectedItems]
  );

  const {
    Title: title,
    Year: year,
    Type: type,
    Poster: poster,
    imdbID,
  } = props;

  const posterAltText = `${title} poster`;

  const isSelected = selectedItems.has(imdbID);

  return (
    <Card
      alt-label={isSelected ? "selected" : "not selected"}
      isSelected={isSelected}
      onClick={() => onSelectItem({ ...props })}
    >
      {poster != null && poster !== "" && poster !== "N/A" ? (
        <PosterImage alt={posterAltText} src={poster} />
      ) : (
        <PosterImagePlaceholder>No Poster Image</PosterImagePlaceholder>
      )}
      <CardContent>
        <CardTextLabel label="Title" value={title} />
        <CardTextLabel label="Year" value={year} />
        <CardTextLabel label="Type" value={type} />
      </CardContent>
    </Card>
  );
}

export default SearchResultCard;
