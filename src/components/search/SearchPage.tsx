import type { SearchResponse } from "./SearchQueryTypes";

import React, { useState } from "react";
import styled from "styled-components";
import { sendQuery } from "../../utils/omdbUtils";

import ReviewCartButton from "./ReviewCartButton";
import SearchResultsView from "./SearchResultsView";
import PageLayout from "../PageLayout";
import Logo from "../Logo";
import Button from "../Button";

const HeaderContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  @media (max-width: 414px) {
    flex-direction: column;
    align-items: stretch;
  }
`;

const SearchBarContainer = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: center;
  border: 1px solid lightgray;
  padding: 4px;
  border-radius: 8px;
`;

const SearchBarInput = styled.input`
  padding: 8px;
  max-width: 400px;
  flex-grow: 1;
  font-size: 16px;
  border: 0;
  outline: 0;
`;

const SearchBarSubmitButton = styled(Button)`
  box-sizing: border-box;
  background-color: dodgerblue;
  color: white;
  padding: 8px;
  font-size: 16px;
  border-radius: 8px;
  :active {
    background-color: cornflowerblue;
  }
`;

function SearchPage() {
  const [query, setQuery] = useState("");
  const [isQuerying, setIsQuerying] = useState(false);
  const [queryErrored, setQueryErrored] = useState(false);
  const [queryData, setQueryData] = useState<SearchResponse>(null);

  const resetQuery = () => {
    setQueryErrored(false);
    setQueryData(null);
  };

  const handleSubmitQuery = async (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault(); // stop page from reloading on form submit
    resetQuery();
    setIsQuerying(true);
    try {
      const queryResponse = await sendQuery(query);

      if (queryResponse?.status === 200) {
        setQueryData(queryResponse.data);
      } else {
        // set error if we received an unexpected response
        setQueryErrored(true);
      }
    } catch (err) {
      // set error if we encountered an error
      setQueryErrored(true);
    }
    setIsQuerying(false);
  };

  return (
    <PageLayout
      header={
        <HeaderContainer>
          <Logo />
          <form onSubmit={handleSubmitQuery}>
            <SearchBarContainer>
              <SearchBarInput
                type="text"
                value={query}
                onChange={(event) => setQuery(event.target.value)}
              />
              <SearchBarSubmitButton type="submit">
                Search
              </SearchBarSubmitButton>
            </SearchBarContainer>
          </form>
        </HeaderContainer>
      }
      footer={<ReviewCartButton />}
    >
      <SearchResultsView
        isErrored={queryErrored}
        isQuerying={isQuerying}
        queryData={queryData}
      />
    </PageLayout>
  );
}

export default SearchPage;
