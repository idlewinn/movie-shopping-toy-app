import { useContext } from "react";
import { useHistory } from "react-router";
import styled from "styled-components";
import Button from "../Button";
import { MovieContext } from "../MovieContextProvider";

const CheckoutCart = styled(Button)`
  background-color: ${(props) => (props.disabled ? "lightgray" : "green")};
  color: white;
  font-weight: bold;
  padding: 8px;
  height: 60px;
  :active {
    background-color: lightgreen;
  }
  @media (min-width: 415px) {
    width: 200px;
    align-self: flex-end;
  }
`;

interface Props {
  selectedIDs: Set<string>;
}
function CheckoutCartButton({ selectedIDs }: Props) {
  const { setSelectedItems } = useContext(MovieContext);
  const history = useHistory();
  const itemCount = selectedIDs.size;
  const itemsText = itemCount !== 1 ? "items" : "item";

  return (
    <CheckoutCart
      disabled={itemCount === 0}
      onClick={() => {
        console.log("Checked out with these items: ", selectedIDs);
        // clear out existing items on checkout normally we'd wait
        // till success confirmed on backend but this simulates it.
        setSelectedItems(new Map());
        history.push("/confirmation");
      }}
    >
      Check out {itemCount} {itemsText}
    </CheckoutCart>
  );
}

export default CheckoutCartButton;
