import styled from "styled-components";

import CartItemCard from "./CartItemCard";
import CenteredContainer from "../CenteredContainer";
import CenteredText from "../CenteredText";
import { SearchResult } from "../search/SearchQueryTypes";
import { useEffect, useRef } from "react";

const ContentContainer = styled(CenteredContainer)`
  @media (min-width: 415px) {
    align-content: stretch;
    flex-direction: row;
    flex-wrap: wrap;
  }
`;

const Banner = styled.div`
  border: 1px solid lightgray;
  border-radius: 8px;
  margin: 8px;
  background-color: white;
  padding: 8px;
`;

interface Props {
  cartItems: SearchResult[];
  selectedIDs: Set<string>;
  onToggleID: (imdbID: string) => void;
}

function CartView({ cartItems, selectedIDs, onToggleID }: Props) {
  const bannerRef = useRef<HTMLDivElement | null>(null);

  useEffect(() => {
    bannerRef.current?.scrollIntoView(false);
  }, []);

  if (cartItems.length === 0) {
    return (
      <CenteredText>
        <i>Your cart is empty.</i>
      </CenteredText>
    );
  }
  return (
    <>
      <Banner ref={bannerRef}>
        <b>Please confirm which items you wish to purchase.</b>
      </Banner>
      <ContentContainer>
        {cartItems.map((item) => (
          <CartItemCard
            key={item.imdbID}
            isSelected={selectedIDs.has(item.imdbID)}
            onToggleID={onToggleID}
            {...item}
          />
        ))}
      </ContentContainer>
    </>
  );
}

export default CartView;
