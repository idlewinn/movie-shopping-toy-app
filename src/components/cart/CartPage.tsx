import { useCallback, useContext, useState } from "react";

import PageLayout from "../PageLayout";
import CheckoutCartButton from "./CheckoutCartButton";
import CartView from "./CartView";
import Logo from "../Logo";
import { MovieContext } from "../MovieContextProvider";
import styled from "styled-components";
import CenteredContainer from "../CenteredContainer";
import ReturnHomeButton from "../confirmation/ReturnHomeButton";

const HeaderContainer = styled.div`
  display: flex;
  align-items: center;
  flex-direction: column;
  align-items: stretch;
`;

const CartHeader = styled(CenteredContainer)`
  text-align: center;
  font-size: 24px;
`;

function CartPage() {
  const { selectedItems } = useContext(MovieContext);
  const [selectedIDs, setSelectedIDs] = useState(new Set(selectedItems.keys()));

  const onToggleID = useCallback(
    (imdbID: string) => {
      const updatedSelectedIDs = new Set(selectedIDs);
      if (selectedIDs.has(imdbID)) {
        updatedSelectedIDs.delete(imdbID);
      } else {
        updatedSelectedIDs.add(imdbID);
      }
      setSelectedIDs(updatedSelectedIDs);
    },
    [selectedIDs, setSelectedIDs]
  );

  const cartItems = Array.from(selectedItems.values());

  return (
    <PageLayout
      header={
        <HeaderContainer>
          <Logo />
          <CartHeader>CART</CartHeader>
        </HeaderContainer>
      }
      footer={
        selectedItems.size === 0 ? (
          <ReturnHomeButton />
        ) : (
          <CheckoutCartButton selectedIDs={selectedIDs} />
        )
      }
    >
      <CartView
        cartItems={cartItems}
        selectedIDs={selectedIDs}
        onToggleID={onToggleID}
      />
    </PageLayout>
  );
}

export default CartPage;
