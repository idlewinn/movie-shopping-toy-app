import styled from "styled-components";
import Button from "../Button";
import type { SearchResult } from "../search/SearchQueryTypes";
import CardTextLabel from "../CardTextLabel";

const Card = styled(Button)`
  align-items: center;
  min-width: 300px;
  border: 1px solid lightgray;
  border-radius: 8px;
  background-color: white;
  margin: 0 8px 8px 8px;
  display: flex;
  flex-grow: 1;
  flex-shrink: 1;
  flex-basis: 0;
  padding: 8px;
`;

const Checkbox = styled.input`
  flex-shrink: 0;
  width: 24px;
  height: 24px;
`;

const CardContent = styled.div`
  flex-grow: 1;
  margin: 16px;
  text-align: start;
`;

const PosterImage = styled.img`
  margin: 8px;
  height: 160px;
  object-fit: contain;
  max-width: 30%;
`;

const PosterImagePlaceholder = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  padding: 8px;
  height: 160px;
  border: 1px solid gray;
  width: 30%;
`;

interface Props extends SearchResult {
  isSelected: boolean;
  onToggleID: (imdbID: string) => void;
}

function CartItemCard({
  Title: title,
  Year: year,
  Type: type,
  Poster: poster,
  imdbID,
  isSelected,
  onToggleID,
}: Props) {
  const posterAltText = `${title} poster`;

  return (
    <Card
      aria-label={isSelected ? "selected" : "not selected"}
      onClick={() => onToggleID(imdbID)}
    >
      <Checkbox
        type="checkbox"
        tabIndex={-1}
        readOnly={true}
        checked={isSelected}
      />
      {poster != null && poster !== "" && poster !== "N/A" ? (
        <PosterImage alt={posterAltText} src={poster} />
      ) : (
        <PosterImagePlaceholder>No Poster Image</PosterImagePlaceholder>
      )}
      <CardContent>
        <CardTextLabel label="Title" value={title} />
        <CardTextLabel label="Year" value={year} />
        <CardTextLabel label="Type" value={type} />
      </CardContent>
    </Card>
  );
}

export default CartItemCard;
